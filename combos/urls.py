from django.urls import path

from combos.views import list_combos, my_combos

urlpatterns = [
    path("combos/", list_combos, name="list_combos"),
    path("my_combos/", my_combos, name="my_combos"),
    path("", list_combos, name="home"),
]
