from django.db import models
from django.conf import settings

# Create your models here.
class Combo(models.Model):
    name = models.CharField(max_length = 200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="combos",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
