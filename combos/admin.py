from django.contrib import admin

# Register your models here.
from combos.models import Combo

@admin.register(Combo)
class ComboAdmin(admin.ModelAdmin):
    list_display=(
        "name",
        "description",
        "owner",
        "id",
    )
