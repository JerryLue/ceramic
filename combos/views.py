from django.shortcuts import render, redirect
from combos.models import Combo
from django.contrib.auth.decorators import login_required

# Create your views here.
def list_combos(request):
    combos_list = Combo.objects.all()
    context = {
        "combos_list": combos_list,
    }
    return render(request, "combos/list_combos.html", context)


login_required
def my_combos(request):
    my_combos_list = Combo.objects.filter(owner=request.user)
    context = {
        "my_combos_list": my_combos_list,
    }
    return render(request, "combos/my_combos.html", context)
