from django.shortcuts import render, redirect
from glazes.forms import GlazeForm
from glazes.models import Glaze
from django.contrib.auth.decorators import login_required

@login_required
# define a function to make a new project task
def create_glaze(request):
    if request.method == "POST":
        form = GlazeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("glaze_list")
    else:
        form = GlazeForm()
    context = {"form": form}
    return render(request, "glazes/create.html", context)
