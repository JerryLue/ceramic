from django.forms import ModelForm

from glazes.models import Glaze


class GlazeForm(ModelForm):
    class Meta:
        model = Glaze
        fields = (
            "name",
            "color",
        )
