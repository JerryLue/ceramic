from django.urls import path

from glazes.views import create_glaze


urlpatterns = [
    path("create/", create_glaze, name="create_glaze"),
]
