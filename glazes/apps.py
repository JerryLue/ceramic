from django.apps import AppConfig


class GlazesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "glazes"
