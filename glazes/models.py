from django.shortcuts import render
from django.db import models
from django.conf import settings


color_choices = (
    ("red","red"),
    ("blue","blue"),
    ("green","green"),
    ("yellow","yellow"),
    ("purple","purple"),
    ("orange","orange"),
    ("black","black"),
    ("white","white"),
    ("gray","gray"),
    ("brown","brown"),
    ("turquoise","turquoise"),
    ("pink","pink"),
    ("clear","clear"),
    ("none","none"),
)

class Glaze(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(
        max_length=20,
        choices=color_choices,
        default="none",
        )
