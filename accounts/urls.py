from django.urls import path

from accounts.views import user_login, user_logout, signup

# path(location/html path, views function that was imported,
# name=usually same name as views function but can be changed depending
# on how you want to call the url)
urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
